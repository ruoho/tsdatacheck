import os
import unittest
import sys

import numpy as np

# Adding the program directory to the search path
MODULE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..',
    'tsdatacheck')
sys.path.insert(0, MODULE_DIR)

from datatypes import TimeSeries

class TestMethods(unittest.TestCase):
    """Test class for unit testing."""

    def setUp(self):
        """Setting up an environment for every test."""
        
        self.dfs = [TimeSeries('tests/test_data_timeseries.csv'),
            TimeSeries('tests/test_data_trajectory.csv')]

    def test_read_file(self):
        """Testing reading data from cruise files (.mit)."""
        
        # Test the number of rows
        self.assertEqual(self.dfs[0].data.shape, (5760,))
        self.assertEqual(self.dfs[1].data.shape, (2454,))
        # Test the number of variables
        self.assertEqual(len(self.dfs[0].variables), 8)
        self.assertEqual(len(self.dfs[1].variables), 8)
        # Test the column names
        self.assertEqual(self.dfs[0].data.dtype.names[2], 'QC_Temp')
        self.assertEqual(self.dfs[1].data.dtype.names[2], 'Lon')
        # Test a single value
        self.assertEqual(self.dfs[0].data['Sal'][5], 6.5531)
        self.assertEqual(self.dfs[1].data['Sal'][5], 5.1320)

    def test_save_file(self):
        """Test all possible write-read combinations with different column
        separators.
        
        """
        
        ftypes = ['csv', 'tsv', 'txt']
        for s in ftypes:
            fname = '.temp.' + s
            for df in self.dfs:
                df.write_data(fname)
                temp = TimeSeries(fname)
                for t in ftypes:
                    fname2 = '.temp.' + t
                    temp.write_data(fname2)
                    temp = TimeSeries(fname2)
                    os.remove(fname2)
            if os.path.exists(fname):
                os.remove(fname)
    
    def test_remove_flagged(self):
        for df in self.dfs:
            bad = df.data['QC_Temp'] == 4
            # Test the data before removal
            self.assertFalse(df.data['Temp'][bad].mask[0])
            self.assertEqual(df.data['QC_Temp'][bad][0], 4)
            # Test if the bad data is overwritten with empty values when applying
            # the removal method
            df.remove_flagged()
            self.assertTrue(df.data['Temp'][bad].mask[0])
            self.assertEqual(df.data['QC_Temp'][bad][0], 9)


if __name__ == "__main__":
    unittest.main()
