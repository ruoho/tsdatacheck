# TSDataCheck

This is a small GUI software for the manual quality control of different
time-series data files. The files are common column-based text files, with
quality flag columns added for each variable.

Written by:<br/>
Jani Ruohola, Finnish Environment Institute SYKE<br/>
Jonni Lehtiranta, Finnish Meteorological Institute FMI

Note! Works only with numpy versions 1.14.0-> and matplotlib versions 2.1.2->

