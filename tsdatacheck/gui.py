from datetime import datetime, timedelta
import io
import os
import sys
from time import time

from PyQt5 import QtWidgets, QtGui, QtCore

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as ticker
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT
from matplotlib.figure import Figure
from matplotlib.widgets import SpanSelector
import time

from configparser import ConfigParser

from about import about_text
from datatypes import TimeSeries, Diary
from datasources import TestDataSource, dataSources
from include import ROOT_DIR

from guitools import DataFlagger, Toolbar

class GUI(QtWidgets.QMainWindow):
    """Create the main window of Qt and handle all user interaction with the
    program.
    """

    configFileName = "config/tsdatacheck.cfg"

    def __init__(self):
        self.files = []
        self.ts = None
        self.figure = None
        self.canvas = None
        self.config = None
        self.toolbar = None
        self.layout = None
        self.function_menu = None
        self.window_title = None
        self.edited = False
        self.flagged = {}
        super().__init__()
        self.__read_config()
        self.__init_window()
        self.__init_menus()
        self.__init_plots()

    def __init_window(self):
        # Set the window layout
        self.setCentralWidget(QtWidgets.QWidget())
        self.layout = QtWidgets.QVBoxLayout()
        self.centralWidget().setLayout(self.layout)
#        self.showMaximized()
        self.__set_window_title('TSDataCheck')
        self.setWindowIcon(QtGui.QIcon(os.path.join(ROOT_DIR,'images',
            'syke_logo.png')))
        self.setGeometry(100, 1000, 1280, 1024)

        # Open as a centered window -- https://pythonprogramminglanguage.com/pyqt5-center-window/
        qtRectangle = self.frameGeometry()
        centerPoint = QtWidgets.QDesktopWidget().availableGeometry().center()
        qtRectangle.moveCenter(centerPoint)
        self.move(qtRectangle.topLeft())

    def __read_config(self):
        '''Reads the configuration file and returns the option values as a
        ConfigParser object.'''
        self.config = ConfigParser(allow_no_value = True)
        self.config.optionxform = str
        self.config.read(self.configFileName)
        print("CFG: "+self.config.get("GUI", "plot_title_location"))


    def __set_window_title(self, text):
        self.window_title = text
        super().setWindowTitle(text)

    def update_window_title(self):
        if not self.edited:
            self.edited = True
            self.window_title += ' *'
            self.setWindowTitle(self.window_title)

    def __init_menus(self):
        # Create different actions for the menubar items
        open_action = QtWidgets.QAction('&Open', self)
        open_action.setShortcut('Ctrl+O')
        open_action.triggered.connect(self.__open_dialog)

        append_action = QtWidgets.QAction('&Append', self)
        append_action.triggered.connect(self.__open_dialog)

        save_action = QtWidgets.QAction('&Save', self)
        save_action.setShortcut('Ctrl+S')
        save_action.triggered.connect(self.__open_dialog)

        exit_action = QtWidgets.QAction('&Quit', self)
        exit_action.setShortcut('Ctrl+Q')
        exit_action.triggered.connect(self.close)

        mask_action = QtWidgets.QAction('&Apply time mask from file', self)
        mask_action.triggered.connect(self.__open_dialog)

        remove_action = QtWidgets.QAction('&Remove flagged data', self)
        remove_action.setShortcut('Ctrl+R')
        remove_action.triggered.connect(self.__open_dialog)

        clear_action = QtWidgets.QAction('&Reset flags', self)
        clear_action.triggered.connect(self.__open_dialog)

        view_masked_toggle = QtWidgets.QAction('&View masked data', self, checkable=True)
        view_masked_toggle.setStatusTip('Whether to view masked data and scale y axis to include it')
        if self.config.get("GUI", "plot_view_masked") == 'True':
            view_masked_toggle.setChecked(True)
        else:
            view_masked_toggle.setChecked(False)
        view_masked_toggle.triggered.connect(self.__toggle_view_masked)

        about_action = QtWidgets.QAction('&About', self)
        about_action.triggered.connect(self.__open_dialog)

        # Add the menubar and the items to it
        menubar = self.menuBar()
        file_menu = menubar.addMenu('&File')
        file_menu.addAction(open_action)
        file_menu.addAction(append_action)
        file_menu.addAction(save_action)
        file_menu.addAction(exit_action)
        qc_menu = menubar.addMenu('&Quality control')
        qc_menu.addAction(mask_action)
        qc_menu.addAction(remove_action)
        self.function_menu = qc_menu.addMenu('&QC functions')
        view_menu = menubar.addMenu('&View')
        view_menu.addAction(clear_action)
        view_menu.addAction(view_masked_toggle)
        help_menu = menubar.addMenu('&Help')
        help_menu.addAction(about_action)


    def __init_plots(self):
        """Create PyQtGraph PlotWidgets for plotting."""

        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = Toolbar(self.canvas, self)
        self.layout.addWidget(self.canvas)
        self.layout.addWidget(self.toolbar)


    def __open_dialog(self):
        """Open a QDialog window and calls corresponding functions based on the
        sender.
        """

        sender = self.sender()
        # Test which sender sent the signal
        try:
            if sender.text() == '&Open':
                fname = QtWidgets.QFileDialog.getOpenFileNames(self,
                    'Open data from file',
                    filter = 'Data files (*.txt *.csv *.tsv)')
                if len(fname[0]) > 0:
                # This prevents unexpected behavior when the program executes
                # internal functions that do not need the fname parameter.
                    self.__open_files(fname[0])
            if sender.text() == '&Append':
                fname = QtWidgets.QFileDialog.getOpenFileNames(self,
                    'Append data from file',
                    filter = 'Data files (*.txt *.csv *.tsv)')
                if len(fname[0]) > 0:
                    self.__append_files(fname[0])
            elif sender.text() == '&Save':
                if (len(self.files) == 1):
                    proposed_name = self.files[0]
                else:
                    file0 = self.files[0].rsplit(".", 1)
                    fileN = self.files[-1].rsplit('/',1)[1].rsplit(".", 1) # strip path with first rsplit
                    proposed_name = file0[0]+"-"+fileN[0]+"."+file0[1]
                print ("Saving when loaded {} files, proposing name {}".format(len(self.files), proposed_name))
                fname = QtWidgets.QFileDialog.getSaveFileName(self,
                    'Save data to file', proposed_name,
                   filter =  'Data files (*.txt *.csv *.tsv)')
                if len(fname[0]) > 0:
                    self.__save_file(fname[0])
            elif sender.text() == '&Apply time mask from file':
                fname = QtWidgets.QFileDialog.getOpenFileName(self,
                    'Apply time mask from file',
                    filter = 'Mask files (*.txt *.csv *.tsv)')
                if len(fname[0]) > 0:
                    self.__apply_mask(fname[0])
            elif sender.text() == '&Remove flagged data':
                self.__remove_flagged()
            elif sender.text() == '&Reset flags':
                self.__read_flags()
                self.update_flags()
            elif sender.text() == '&About':
                QtWidgets.QMessageBox.about(self, 'About', about_text)
        except: # If the dialog is closed without return values
            pass

    def __toggle_view_masked(self):
        if self.config.get("GUI", "plot_view_masked") == 'True':
            self.config.set("GUI", "plot_view_masked", 'False')
        else:
            self.config.set("GUI", "plot_view_masked", 'True')
        self.__replot()


    def __open_files(self, fnames):
        '''Read data from one file or a list of files at the same time'''
        self.ts = None  ## If some data was open, remove it

        if type(fnames) is list:
            for fname in fnames:
                self.__open_file(fname, message_box=False, plot=False)
            self.__replot('TSDataCheck - ' + fname)
        else:
            self.__open_file(fnames)


    def __open_file(self, fname, message_box=True, plot=True):
        '''
        Read data from the given file, appending to current data if it exists
        '''
        if self.ts != None:     ## A file is already open!
            self.__append_file(fname, message_box=False, plot=plot)
            return

        fformat = ""
        print ("Opening file {}".format(fname))
        for ds in dataSources:
            if ds.detect(fname):
                print("File format detected as {}".format(ds))
                fformat=ds.description
                self.ts = ds.readFile(fname)
                self.__add_qc_functions(self.ts.qc_functions)
                self.files.append(fname)
                self.__read_flags()

                ddiary = ds.readDefaultDiary()
                if ddiary:
                    self.__apply_mask(io.StringIO(ddiary), message_box=False)

                for d in self.ts.diaries:
                    self.__apply_mask(d, message_box=False)
                break
        else: # The for loop did not find a file format
            print("File format not detected for file {}".format(fname))
            return

        if plot:
            self.__replot('TSDataCheck - ' + fname)
        if message_box:
            text = 'Data file {} opened successfully as '.format(fname) + fformat
            QtWidgets.QMessageBox.information(self, 'Information', text)


    def __append_files(self, fnames, message_box=False, plot=True):
        if type(fnames) is list:
            for fname in fnames:
                self.__append_file(fname, message_box, plot=False)
            self.__replot('TSDataCheck - ' + ", ".join(fnames))
        else:
            self.__append_file(fnames, message_box, plot=True)


    def __append_file(self, fname, message_box=False, plot=True):
        fformat = ""
        ddiary = None
        new_ts = TimeSeries()
        print ("Opening file {}".format(fname))
        for ds in dataSources:
            if ds.detect(fname):
                print("File format detected as {}".format(ds))
                fformat=ds.description
                new_ts = ds.readFile(fname)
                self.files.append(fname)
                ddiary = ds.readDefaultDiary()
                break
        else: # The for loop did not find a file format
            print("File format not detected")
            return
        self.ts.append_ts(new_ts)
        if ddiary:
            self.__apply_mask(io.StringIO(ddiary), message_box=False)
        for d in new_ts.diaries:
            self.__apply_mask(d, message_box=False)
        if plot:
            self.__replot('TSDataCheck - ' + fname)
        if message_box:
            text = 'Data file {} opened successfully as '.format(fname) + fformat
            QtWidgets.QMessageBox.information(self, 'Information', text)


    def __replot(self, window_title=None):
        print ("Plotting data")
        self.__plot()
        if window_title:
            self.__set_window_title(window_title)
        self.update_flags()


    def __save_file(self, fname):
        """Save the edited data to the given file."""

        self.files = [fname]
        self.__write_flags()
        self.ts.write_data(fname)
        self.edited = False
        self.__set_window_title('TSDataCheck - ' + fname)


    def __read_flags(self):
        """Initialize the dictionary for flagged data."""

        for var in self.ts.variables:
            sel = np.where(self.ts.data['QC_' + var] == 4)[0]
            self.flagged[var] = sel


    def __write_flags(self):
        """Write the flagging dictionary to the TimeSeries instance, replacing
        the original quality flags.
        """

        for var in self.flagged:
            not_empty = np.where(self.ts.data['QC_' + var] != 9)[0]
            for i in not_empty:
                if i in self.flagged[var]:
                    self.ts.data['QC_' + var][i] = 4
                else:
                    self.ts.data['QC_' + var][i] = 1


    def __apply_mask(self, fname, message_box=True):
        """Mask out time intervals based on start and end times of service
        breaks or other periods of questionable measurements.
        fname can be a file name, a string containing time intervals in ascii,
        or a Numpy.ndarray containing tuples of time intervals.
        """
        print("Input type is {}".format(type(fname)))
        if (type(fname) is np.ndarray):  # Hacky hacky hack! ow ow
            df = fname
        elif (type(fname) is Diary):
            df = fname.intervals
        else:
            sep = self.ts.field_separator # get_delimiter(fname)
            df = np.genfromtxt(fname, delimiter=sep, usemask=False,
                names=True, dtype=['datetime64[s]'] * 2).astype('O')
        count = 0
        for i in range(self.ts.data.size):
            t = self.ts.time[i]
            for mask_t in df:
                if t >= mask_t[0] and t <= mask_t[1]:
                    for var in self.flagged:
                        if i not in self.flagged[var] and \
                            self.ts.data['QC_' + var][i] != 9:
                            self.flagged[var] = np.append(self.flagged[var], i)
                            count += 1
        self.update_window_title()
        self.update_flags()
        if message_box:
            text = 'Time mask applied, {} measurements marked as bad.'.format(
                count)
            QtWidgets.QMessageBox.information(self, 'Information', text)


    # __def qc_function_dialog(self, question_text, qc_function):
    #     reply = QtWidgets.QInputDialog.getText(self, 'Question',
    #         "Please insert the RANGE")
    #         # , QtWidgets.QMessageBox.Yes |
    #         # QtWidgets.QMessageBox.No)
    #     return reply


    def __add_qc_functions(self, qc_actions):
        """Read available QC functions from the TimeSeries object and pass
        them to function_menu.
        """
        if len(self.function_menu.actions()) > 0:
            for a in self.function_menu.actions():
                self.function_menu.removeAction(a)
        for f in qc_actions:
            # Quick way to get a function name, probably should write them in a class
            a = QtWidgets.QAction('&{}'.format(f.__name__), self)
            a.func = f
            a.triggered.connect(self.__run_qc_function)
            self.function_menu.addAction(a)


    def __run_qc_function(self):#, message_box = True):
        # For some reason the kwarg message_box is False after call by QAction
        sender = self.sender()
        count = 0
        for var in self.ts.variables:
            qf = sender.func(self.ts.data[var])
            for i in range(qf.size):
                if not qf[i] and i not in self.flagged[var] and \
                    self.ts.data['QC_' + var][i] != 9:
                    self.flagged[var] = np.append(self.flagged[var], i)
                    count += 1
        self.update_window_title()
        self.update_flags()
        # if message_box:
        text = 'QC function {} applied, {} measurements marked as bad.'.format(
            sender.func.__name__, count)
        QtWidgets.QMessageBox.information(self, 'Information', text)


    def __remove_flagged(self):
        self.__write_flags()
        self.ts.remove_flagged()
        self.update_window_title()
        self.__read_flags()
        self.__plot()


    def __plot(self, varlist = None):
        """Plot all variables."""

        # Clear the plot, in case of one is already drawn
        self.figure.clear()
        x = self.ts.time
        timespan = x[-1] - x[0]
        if varlist is None:
            varlist = list(self.ts.variables.keys())
        elif type(varlist) != list:
            varlist = [varlist]
        plots = len(varlist)
        mpl.rcParams.update({'font.size': int(12 - np.ceil(np.log(plots)))})
        tick_interval = int(np.ceil(1 + 2 * np.log(plots)))
        rows = int(np.ceil(np.sqrt(plots)))
        cols = int(np.ceil(plots/rows))
        for i in range(plots):
            var = varlist[i]
            y = self.ts.data[var]

            #JL: If this variable is all NaNs, and plot_view_empty is False, skip this var
            if self.config.get("GUI", "plot_view_empty") == 'False' and np.isnan(np.nanmin(y)):
                print ("Variable {} all NaNs, not drawing".format(var))
                continue

            #JL: Y axis to cover all values, or only unmasked ones??
            if self.config.get("GUI", "plot_view_masked") == 'True':
                y.mask = False
                y.mask[self.ts.data['QC_'+var] == 9] = True
            else:
                y.mask = False
                y.mask[self.flagged[var]] = True
                y.mask[self.ts.data['QC_'+var] == 9] = True
            ymin = self.ts.variables[var]['min']
            ymax = self.ts.variables[var]['max']
            if ymin is None:
                ymin = np.nanmin(y)
                if np.isnan(ymin): #JL# Because NaN doesn't work for plotting
                    ymin = -1
            if ymax is None:
                ymax = np.nanmax(y)
                if np.isnan(ymax):
                    ymax = 1
            ax = self.figure.add_subplot(rows, cols, i + 1)
            ax.variable = var
            ax.plot(x, y, color='r', linewidth=1)
            if self.ts.variables[var]['unit'] != '':
                unitstr = ' [{}]'.format(self.ts.variables[var]['unit'])
            else:
                unitstr = ''
            #JL: put title to left or top, according to config file (default:left)
            if self.config.get("GUI", "plot_title_location") == "top":
                ax.set_title(self.ts.variables[var]['name'])
                ax.set_ylabel(unitstr, labelpad = 0)
            elif self.config.get("GUI", "plot_title_location") == "left":
                ax.set_ylabel('{}{}'.format(self.ts.variables[var]['name'], unitstr), labelpad = 10)
            else:
                ax.set_ylabel('{}{}'.format(self.ts.variables[var]['name'], unitstr), labelpad = 10)
            ax.set_ylim(ymin, ymax)
            ax.grid(linestyle = '-', linewidth = 1, alpha = 0.25)
            # Format time axis
            xax = ax.get_xaxis()
            xax.set_tick_params(which = 'major', pad = 20, length = 0) #JL# 25
            xax.set_tick_params(which = 'minor', pad = 3, length = 2) #JL# 5
            if timespan <= timedelta(days = 1):
                xax.set_major_locator(mdates.HourLocator(byhour = 12))
                xax.set_major_formatter(mdates.DateFormatter('%a, %b %d'))
                xax.set_minor_locator(mdates.HourLocator(interval = int(tick_interval *0.8)))
                xax.set_minor_formatter(mdates.DateFormatter('%H:%S'))
            elif timespan <= timedelta(days = 7):
                xax.set_major_locator(mdates.HourLocator(byhour = 12))
                xax.set_major_formatter(mdates.DateFormatter('%a, %b %d'))
                xax.set_minor_locator(mdates.HourLocator(interval = tick_interval * 6)) #JL# 7
                xax.set_minor_formatter(mdates.DateFormatter('%H:%S'))
            elif timespan < timedelta(days = 30):
                xax.set_major_locator(mdates.DayLocator(bymonthday = 15))
                xax.set_major_formatter(mdates.DateFormatter('%b'))
                xax.set_minor_locator(mdates.DayLocator(interval = tick_interval))
                xax.set_minor_formatter(mdates.DateFormatter('%d'))
            elif timespan < timedelta(days = 365):
                xax.set_major_locator(mdates.DayLocator(bymonthday = 15))
                xax.set_major_formatter(mdates.DateFormatter('%b %y'))
                xax.set_minor_locator(mdates.DayLocator(bymonthday = [1, 5, 10, 15, 20, 25]))
                xax.set_minor_formatter(mdates.DateFormatter('%d'))
            # Open the SpanSelector widget for manual flagging
            ax.data_flagger = DataFlagger(ax, self)
            # Update the flag color
            self.toolbar.update_flag_color()
        # Prevent the axis titles and subplot areas from overlapping
        self.figure.tight_layout()
        self.canvas.draw()


    def update_flags(self, axes = None):
        """Paint the areas that are QC flagged as 'bad'.

        This method is called everytime the flag dictionary is updated.
        """

        x = self.ts.time
        if axes is None:
            axes = self.figure.axes
        elif type(axes) != list:
            axes = [axes]
        for ax in axes:
            for line in ax.collections:
                line.remove()
            ax.vlines(x[self.flagged[ax.variable]],
                ymin = -10000,
                ymax = 10000,
                color = 'orange')
        self.canvas.draw()


    def closeEvent(self, event):
        """Prompt the user with a confirmation dialog when quitting the program.
        """

        reply = QtWidgets.QMessageBox.question(self, 'Question',
            "Are you sure you want to quit?", QtWidgets.QMessageBox.Yes |
            QtWidgets.QMessageBox.No)
        if reply == QtWidgets.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


    def close(self):
        fout = open(self.configFileName, "w+")
        self.config.write(fout, space_around_delimiters=True)
        fout.close()
        super().close()
