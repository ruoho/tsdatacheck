import numpy as np

class QCFunction:
    name = ''
    variablename = ''
    description = ''

    @staticmethod
    def run():
        pass


class RangeTest:
    name = 'Range test'
    variablename = 'range'
    description = 'Enter range like \'-1;20\''

    @staticmethod
    def run(a, lower = 0, upper = 10):
        """Simple range test implementation for testing.

        TODO:
        -- connect ranges to different variables
        """
        qf = np.ones(a.size, dtype = bool)
        for i in range(a.size):
            if lower <= a[i] <= upper:
                qf[i] = True
            elif not a.mask[i]:
                qf[i] = False
        return qf


# range_between ?
def range_test(a, lower = 0, upper = 10):
    """Simple range test implementation for testing.

    TODO:
    -- connect ranges to different variables
    """
    qf = np.ones(a.size, dtype=bool)
    for i in range(a.size):
        if lower <= a[i] <= upper:
            qf[i] = True
        elif not a.mask[i]:
            qf[i] = False
    return qf
