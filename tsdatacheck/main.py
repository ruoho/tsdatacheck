#!/usr/bin/env python3

import os
import sys

from PyQt5.QtWidgets import QApplication

from gui import GUI


def main():
    global app
    app = QApplication(sys.argv)
    win = GUI()
    win.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
