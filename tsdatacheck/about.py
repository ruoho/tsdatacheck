about_text = """
This is a small GUI program for plotting quality checked time series data and manually editing the quality flags.
Original author Jani Ruohola, Finnish Environment Institute 2018.
"""
