import io
import sys

from configparser import ConfigParser
import numpy as np
import numpy.lib.recfunctions as rfn

import qc_functions as qc
import re

class TimeSeries:
    '''
    This is an object for holding time series data and metadata read
    from time series files. The first column of the data file must be named
    'Time' and the timestamps must be in the ISO 8601 format
    (YYYY-MM-DDTHH:MM:SSZ for UTC, replace Z by +HH:MM for timezone, 24-hour)
    https://en.wikipedia.org/wiki/ISO_8601
    '''

    field_separator = ','
    missing_value = -999

    def __init__(self, filename=None):
        self.filename = '' #JL#
        self.header = '' #JL# get_header(file)
        self.sep = TimeSeries.field_separator #JL# get_delimiter(file)
        self.config = None
        self.colnames = []
        self.qc_cols = []
        self.qc_functions = [qc.range_test]
        self.diaries = [] # WORK IN PROGRESS: add all diaries here
        self.variables = {}
        self.dtypes = []
        self.data = None
        self.__timecol = None         # These three variables are
        self.__latcol = None            # used to reference the temporal and spatial
        self.__loncol = None           # columns of the data matrix
        # Initiate the TimeSeries and read the data
        self.__read_ini('parameterdb.ini')

        if filename is not None:
            self.filename = filename
            self.__read_header(filename)
            self.parse_header()
            self.read_data(filename, TimeSeries.missing_value)
            self.time = self.data[self.__timecol].astype('O') # Convert np.datetime64 to datetime.datetime

    def __read_ini(self, file):
        '''
        Reads the configuration file and returns the option values as a
        ConfigParser object.
        '''
        parser = ConfigParser(allow_no_value = True)
        parser.optionxform = str
        parser.read(file)
        self.config = parser

    def parse_header(self, datetime=[['time', 'datetime64[s]']]):
        '''
        Reads the variable names from a time series file.
        '''
        colnames_raw = self.header.strip().split(self.sep) # Strip to remove \n
        print (colnames_raw)
        for i in range(len(colnames_raw)):
            s = colnames_raw[i]
            colname = s.split()[0].split('[')[0] #.split('(')[0]
            self.colnames.append(colname)
            # Test the datatypes
            lcname = colname.lower()
            if datetime[0][0] in lcname: #JL Usually if "time" but configurable
                self.__timecol = colname
                self.dtypes.append(datetime[0][1]) #JL# 'datetime64[s]'
            elif len(datetime) == 2 and datetime[1][0] in lcname:
                self.dtypes.append(datetime[1][1])
            elif 'lat' in lcname:
                self.__latcol = colname
                self.dtypes.append('f8')
            elif 'lon' in lcname:
                self.__loncol = colname
                self.dtypes.append('f8')
            elif 'qc_' in lcname:
                self.qc_cols.append(colname)
                self.dtypes.append('i8')
            else:
                # try:
                #     varname = self.config[colname].get('name')
                #     varmin = self.config[colname].getfloat('min')
                #     varmax = self.config[colname].getfloat('max')
                # except KeyError:
                #     varname = colname
                #     varmin = None
                #     varmax = None
                if (self.config.has_option(colname, 'name') and self.config.has_option(colname, 'min') and
                        self.config.has_option(colname, 'max')):
                    varname = self.config[colname].get('name')
                    varmin = self.config[colname].getfloat('min')
                    varmax = self.config[colname].getfloat('max')
                else:
                    varname = colname
                    varmin = None
                    varmax = None
                if '[' in s:        # Read unit name from between []'s
                    varunit = s[(s.find('[') + 1):(s.find(']'))]
                else:               # Default to empty unit string
                    varunit = ''
                self.variables[colname] = {'name': varname,
                    'unit': varunit, 'min': varmin, 'max': varmax}
                self.dtypes.append('f8')


    def add_qc_cols(self):
        '''
        If the data file to be read lacks some or all of the Quality Control (QC)
        columns, add them.
        '''
        for var in self.variables:
            if "QC_"+var in self.qc_cols:
                print ("[add_qc_cols] {} exists already".format("QC_"+var))
            else:
                print ("[add_qc_cols] {} does not exist, adding".format("QC_"+var))
                self.header = self.header.strip() + ", QC_" + var
                self.qc_cols.append("QC_"+var)
                self.append_col(np.zeros(shape=(self.data.size)),"QC_"+var, 'i8')


    def remove_col(self, colname):
        try:
            col_ind = self.colnames.index(colname)
        except ValueError:
            print ("[TimeSeries.remove_col()] Column not found")
            return False
        headerlist = self.header.split(",")
        del headerlist[col_ind]
        del self.dtypes[col_ind]
        del self.colnames[col_ind]
        self.header = ','.join(headerlist)
        self.data = rfn.drop_fields(self.data, colname)
        del self.variables[colname] # TODO: Check if this exists or not
                                    # ALSO TODO: Remove QC cols too if applicable

    def append_col(self, data, name, dtype, timecol=False):
        self.data = rfn.append_fields(self.data, names=[name], data=[data],
            dtypes=[dtype])
        self.dtypes.append(dtype)
        self.header = self.header.rstrip() + ", " + name
        self.colnames.append(name)
        if timecol:
            self.__timecol = name


    def append_ts(self, new_ts):
        self.data = rfn.stack_arrays((self.data, new_ts.data))
        self.time = self.data[self.__timecol].astype('O')


    def __read_header(self, file):
        with io.open(file, 'r', encoding='utf-8') as f:
            self.header = f.readline().split('\n')[0]


    def get_time_col(self):
        return self.data[self.__timecol]


    def read_data(self, file, missing_values, skip_header=1, delimiter=','):
        '''
        Read the data from the given file.

        Note: the time string parser of NumPy is backward compatible with
        timezone aware ISO 8601 datetimes, but raises a warning and converts
        them to naive UTC datetimes. This may produce problems in the future
        and should be implemented differently at some point.
        '''
        self.data = np.genfromtxt(file, delimiter=delimiter,
            skip_header=skip_header, usemask=True, names=self.colnames,
            dtype=self.dtypes, missing_values=missing_values, autostrip=True,
            encoding='utf-8', deletechars=r"~!@#$%^&*-=+~\|]}[{';: /?.>,<")


    def write_data(self, file, fill_value = -999):
        '''
        Writes the data to the given file.
        '''
        X = self.data.filled(fill_value)
        fmt = []
        for i in self.dtypes:
            if i == 'datetime64[s]':
                fmt.append('%s+00:00')
            elif i == 'f8':
                fmt.append('%.4f')
            elif i == 'i8':
                fmt.append('%d')
        sep_new = TimeSeries.field_separator #JL# get_delimiter(file)
        header_new = self.header.replace(self.sep, sep_new)
        np.savetxt(file, X, fmt, sep_new, newline = '\n',
            header = header_new, comments = '', encoding = 'utf-8')
        self.sep = sep_new
        self.header = header_new


    def var_gt(self, varname, operand):
        return self.data[varname] > operand

    def var_gte(self, varname, operand):
        return self.data[varname] >= operand

    def var_lt(self, varname, operand):
        return self.data[varname] < operand

    def var_lte(self, varname, operand):
        return self.data[varname] <= operand

    def month_in(self, months):
        datamonths = [dt.astype(object).month for dt in self.data[self.__timecol]]
        return [m in months for m in datamonths]


    def flag_by_mask(self, varname, mask, flag=4):
        '''Flag some values based on a mask that is separately calculated.
        varname can be specified as a string, list, or a regular expression
        (re.Pattern).'''
        if type(varname) is re.Pattern:
            for v in self.colnames:
                if varname.match(v):
                    self.data[v].mask[mask] = True
                    self.data['QC_'+v][mask] = flag
        elif type(varname) is list:
            for v in self.colnames:
                if v in varname:
                    self.data[v].mask[mask] = True
                    self.data['QC_'+v][mask] = flag
        elif varname == "all":
            for v in self.variables:
                print ("Variable: {}".format(v))
                self.data[v].mask[mask] = True
                self.data['QC_'+v][mask] = flag
        else:
            self.data[varname].mask[mask] = True
            self.data['QC_'+varname][mask] = flag


    def mask_to_diary(self, mask, pad=None, vars="all", title="", description="",
            lpad=np.timedelta64(0, 's'), rpad=np.timedelta64(0, 's')):
        print ("Running mask_to_diary with a mask length of {}".format(len(mask)))
        maskstate = False
        diaryints = []
        if (pad is not None and lpad == np.timedelta64(0, 's')):
            lpad = pad
        if (pad is not None and rpad == np.timedelta64(0, 's')):
            rpad = pad
        for i in range(0, len(mask)):
#            print ("{} state {} new {}".format(i, maskstate, mask[i]))
            if maskstate == False and mask[i] == True:
                maskbegin = self.data[self.__timecol][i] - lpad
                print("Mask begin at index {} time {}".format(i, maskbegin))
                maskstate = True
            if maskstate == True and mask[i] == False:
                maskend = self.data[self.__timecol][i] + rpad
                print("Mask end at index {} time {}".format(i, maskend))
                maskstate = False
                mytuple = [(maskbegin, maskend)]
                mytuplearr = np.empty(len(mytuple), dtype=object)
                mytuplearr[:] = mytuple
                diaryints = np.append(diaryints, mytuplearr)
        if maskstate == True:  # Mask ended with True
            maskend = self.data[self.__timecol][-1]
            maskstate = False
            mytuple = [(maskbegin, maskend)]
            mytuplearr = np.empty(len(mytuple), dtype=object)
            mytuplearr[:] = mytuple
            diaryints = np.append(diaryints, mytuplearr)
            print("Mask end at index {} time {}".format(i, maskend))
        return Diary(diaryints, vars=vars, title=title, description=description)


    def diary_by_mask(self, varname, mask, title="", description="", pad=None,
            lpad=np.timedelta64(0, 's'), rpad=np.timedelta64(0, 's')):
        '''Select some values based on a mask that is separately calculated
        and return a diary. varname can be specified as a string, list, or
                  a regular expression (re.Pattern).'''
        varlist = []
        if type(varname) is re.Pattern:
            for v in self.colnames:
                if varname.match(v):
                    varlist.append(v)
        elif type(varname) is list:
            varlist = v
        elif varname == "all":
            varlist = "all"
        else:
            varlist = [v]

        return self.mask_to_diary(mask, pad=pad, vars=varlist, title=title,
            description=description, lpad=lpad, rpad=rpad)


    def add_diary(self, new_diary):
        self.diaries.append(new_diary)

    def remove_flagged(self):
        for var in self.variables:
            sel = self.data['QC_' + var] == 4
            self.data[var].mask[sel] = True
            self.data['QC_' + var][sel] = 9


class Diary:
    def __init__(self, intervals=[], vars=[], title="", description=""):
        self.intervals = intervals
        if (type(vars) is str):
            self.vars = [vars]
        else:
            self.vars = vars
        self.title = title
        self.description = description
