import os

import time
import numpy as np

from PyQt5 import QtWidgets, QtGui, QtCore

import matplotlib as mpl
import matplotlib.dates as mdates
from matplotlib.widgets import SpanSelector
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT

from include import ROOT_DIR


class DataFlagger(SpanSelector):
    def __init__(self, ax, parent):
        self.parent = parent
        self.var = ax.variable
        super().__init__(ax, self.__flag_data, 'horizontal', useblit=True)

    def __flag_data(self, tmin, tmax):
        x = self.parent.ts.time
        t = mdates.date2num(x)
        # Check what measurement times are between the flagged dates and not
        # having empty values in the selected variable
        sel = np.where((t >= tmin) & (t <= tmax) &
                       (self.parent.ts.data['QC_' + self.var] != 9))[0]
        if self.parent.toolbar.mode == 'flag good':
            self.parent.flagged[self.var] = np.setdiff1d(
                self.parent.flagged[self.var], sel,
                assume_unique = True)
        elif self.parent.toolbar.mode == 'flag bad':
            self.parent.flagged[self.var] = np.union1d(
                self.parent.flagged[self.var], sel)
        self.parent.update_flags(self.ax)
        self.parent.update_window_title()


class Toolbar(NavigationToolbar2QT):
    """New class for overriding some of the NavigationToolbar2QT's methods.

    Also, it was easier to add new buttons for data flagging by redefining the
    class attribute 'toolitems'.

    """

    # list of toolitems to add to the toolbar, format is:
    # (
    #   text, # the text of the button (often not visible to users)
    #   tooltip_text, # the tooltip shown on hover (where possible)
    #   image_file, # name of the image for the button (without the extension)
    #   name_of_method, # name of the method in NavigationToolbar2 to call
    # )
    toolitems = (
        ('Home', 'Reset original view', 'home', 'home'),
        ('Back', 'Back to previous view', 'back', 'back'),
        ('Forward', 'Forward to next view', 'forward', 'forward'),
        (None, None, None, None),
        ('Pan', 'Pan axes with left mouse, zoom with right', 'move', 'pan'),
        ('Zoom', 'Zoom to rectangle', 'zoom_to_rect', 'zoom'),
        (None, None, None, None),
        ('Flag good', 'Flag data as good by selecting it from plot',
            'flag-green', 'flag_good'),
        ('Flag bad', 'Flag data as bad by selecting it from plot',
            'flag-red', 'flag_bad'),
        (None, None, None, None),
        ('Save', 'Save the figure', 'filesave', 'save_figure'),
      )

    def __init__(self, canvas, parent, coordinates=True):
        super().__init__(canvas, parent, coordinates=True)
        # Toggle the widgetlock so that DataFlagger is not available without
        # selecting first the appropriate tool.
        self.canvas.widgetlock(self)
        self.click_interval = None

    def _init_toolbar(self):
        for text, tooltip_text, image_file, callback in self.toolitems:
            if text is None:
                self.addSeparator()
            else:
                if callback in ['flag_good', 'flag_bad']:
                    icon = QtGui.QIcon(os.path.join(ROOT_DIR, 'images',
                        image_file + '.png'))
                else:
                    icon = self._icon(image_file + '.png')
                a = self.addAction(icon, text, getattr(self, callback))
                self._actions[callback] = a
                if callback in ['zoom', 'pan', 'flag_good', 'flag_bad']:
                    a.setCheckable(True)
                if tooltip_text is not None:
                    a.setToolTip(tooltip_text)

        self.buttons = {}

        # Add the x,y location widget at the right side of the toolbar
        # The stretch factor is 1 which means any resizing of the toolbar
        # will resize this label instead of the buttons.
        if self.coordinates:
            self.locLabel = QtWidgets.QLabel("", self)
            self.locLabel.setAlignment(
                    QtCore.Qt.AlignRight | QtCore.Qt.AlignTop)
            self.locLabel.setSizePolicy(
                QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding,
                                      QtWidgets.QSizePolicy.Ignored))
            labelAction = self.addWidget(self.locLabel)
            labelAction.setVisible(True)

        # reference holder for subplots_adjust window
        self.adj_window = None

        self.setIconSize(QtCore.QSize(24, 24))
        self.layout().setSpacing(12)

    def _update_buttons_checked(self):
        super()._update_buttons_checked()
        self._actions['flag_good'].setChecked(self._active == 'FLAG_GOOD')
        self._actions['flag_bad'].setChecked(self._active == 'FLAG_BAD')

    def _flag_data(self, flag):
        """Generic method for flagging the data good or bad."""

        if self._active == 'FLAG_{}'.format(flag.upper()): # toggle flagging off
            self._active = None
            self.mode = ''
            self.set_message('')
            self.canvas.widgetlock(self)
        else:
            self._active = 'FLAG_{}'.format(flag.upper())
            self.mode = 'flag {}'.format(flag)
            self.update_flag_color()
            self._zoom_mode = None
            if self._idPress is not None:
                self._idPress = self.canvas.mpl_disconnect(self._idPress)
            if self._idRelease is not None:
                self._idRelease = self.canvas.mpl_disconnect(self._idRelease)
            self.canvas.widgetlock.release(self)
            for a in self.canvas.figure.get_axes():
                a.set_navigate_mode(None)
            self.set_message(self.mode)
        self._update_buttons_checked()

    def update_flag_color(self):
        if self._active is None:
            return
        elif self._active == 'FLAG_GOOD':
            col = 'green'
        elif self._active == 'FLAG_BAD':
            col = 'red'
        for ax in self.parent.figure.axes:
            ax.data_flagger.rectprops['facecolor'] = col
            ax.data_flagger.rect.set_edgecolor(col)
            ax.data_flagger.rect.set_facecolor(col)

    def flag_good(self):
        self._flag_data('good')

    def flag_bad(self):
        self._flag_data('bad')

    def pan(self, *args):
        super().pan(*args)
        # Activate the widgetlock again to avoid unnecessary data flagging
        self.canvas.widgetlock(self)

    def press_zoom(self, event):
        super().press_zoom(event)
        # Time the mouse clicks, double-click results in resetting the zoom
        if event.button == 1:
            now = time.time()
            if self.click_interval is not None:
                if self.click_interval - now < 1:
                    self.home()
                    self.click_interval = None
                else:
                    self.click_interval = now
            else:
                self.click_interval = now
        else:
            self.click_interval = None

    def zoom(self, *args):
        super().zoom(*args)
        # Activate the widgetlock again to avoid unnecessary data flagging
        self.canvas.widgetlock(self)
