import io
import re
import sys

import numpy.lib.recfunctions as rfn
import numpy as np

from datatypes import TimeSeries, Diary

"""The DataSource class is the interface between data files and other
sources and the TSDataCheck program. For each different data source,
this class should be inherited to make a derived class more specific
to a data source.

In the minimum, the detect() and readFile() functions need to be
implemented. Both are passed one parameter, the file name. Function
detect() should return True if the file appears to be of the correct
type. Function readFile() should return a TimeSeries object read from
the file.
"""
class DataSource:
    description = 'dataSourceObject'

    @staticmethod
    def readFile(filename):
        print("Running abstract class method - this should not happen, something is broken.")
        return TimeSeries()

    @staticmethod
    def detect(filename):
        print("Running abstract class method - this should not happen, something is broken.")
        return False

    @staticmethod
    def readDefaultDiary():
        return None

    # JL TO BE REMOVED
    # @staticmethod
    # def makeQCDiary(timeserie): # Is this the way after all?
    #     return None

    def __str__(self):
        return type(self).description

# -------------------------------------------------------------------#

class TestDataSource(DataSource):
    description = 'test_data'

    @staticmethod
    def readFile(filename):
        return TimeSeries(filename)

    @staticmethod
    def detect(filename):
        f = open(filename, 'r')
        line1 = f.readline()
        line2 = f.readline()
        f.close()
        ISO8601timepattern = re.compile(
            r"\d\d\d\d-[01]\d-[0-3]\dT[012]\d:[0-5]\d:[0-5]\d\+[0-2]\d:[0-5]\d",
            re.IGNORECASE)
        if line1.startswith('Time,') and ISO8601timepattern.match(line2):
            return True
        return False


# -------------------------------------------------------------------#


class Uto_MicroTSG_DS(DataSource):
    description = 'Utö microTSG'

    @staticmethod
    def detect(filename):
        f = open(filename, 'r')
        line1 = f.readline()
        line2 = f.readline()
        f.close()
        sciencetimepattern = re.compile(
            r"\d\d\d\d-[01]\d-[0-3]\d [012]\d:[0-5]\d:[0-5]\d",
            re.IGNORECASE)
        if line1.startswith('"Period_start",') and sciencetimepattern.match(line2):
            return True
        return False

    @staticmethod
    def readFile(filename):
        result = TimeSeries()
        with io.open(filename, 'r', encoding='utf-8') as f:
            header = f.readline().split('\n')[0]
        result.filename = filename
        header = re.sub(r'[".]', '', header)                # Remove quotation marks
        header = re.sub('Period_start,', 'Time,', header)   # Rename time column
        header = re.sub(r'_([a-zA-Z/]+)(,|$)', '[\\1]\\2', header) # "_unit" to "[unit]"
        header = re.sub(r'#', 'num_', header) # Hash mark not allowed in a field name
        result.header = header
        result.parse_header()
        result.read_data(filename, missing_values=-999.0000)

        result.remove_col("num_data")


        print ("Type of time column: {}".format(type(result.get_time_col())))
        result.time = result.get_time_col().astype('O')
        result.add_qc_cols()

#        QCmask = np.logical_and(result.month_in([1, 2, 3, 4]), result.var_gt("avg(T)", 4.0))
        QCmask = np.logical_and(result.month_in([1, 2, 3, 4]), result.var_gt("avg(T)", 7.0))
        QCdiary = result.mask_to_diary(QCmask, lpad=np.timedelta64(5, 'm'), rpad=np.timedelta64(1, 'h'),
            vars="all", title="Spring T too high", description="average temperature Jan - Apr above 4 C")
        result.add_diary(QCdiary)

#        result.flag_by_mask(re.compile("(min|max)\((T|Sal)\)"), result.var_gt("avg(T)", 4.0))
#        result.flag_by_mask(["avg(T)", "min(T)", "max(T)"], result.var_gt("avg(T)", 4.0))
        # result.flag_by_mask("all",
        #      np.logical_or(result.var_gt("avg(T)", 5.0), result.var_lt("avg(Sal)", 1.0)))

        return result

    @staticmethod
    def readDefaultDiary():
        return Uto_Diary()


def Uto_Diary():
    print ("Called readDefaultDiary for Utö")
    diaryString = io.StringIO()
    diaryString.write("starttime,endtime")
    diarytimepattern = re.compile(
        r"^([0-3]\d?)\.([01]?\d)\.(20\d\d).* ([012]?\d)\:(\d\d)-~?([012]?\d):(\d\d).*$",
            re. IGNORECASE)
    with io.open("tests/Uto_diary_2019.txt", 'r', encoding='iso-8859-1') as f:
        for line in f:
            m = diarytimepattern.match(line.rstrip())
            if m:
                YYYY=m.group(3)
                DD=m.group(1).zfill(2)
                MM=m.group(2).zfill(2)
                HH1=m.group(4).zfill(2)
                MM1=m.group(5)
                HH2=m.group(6).zfill(2)
                MM2=m.group(7)
                diaryitem = YYYY+'-'+MM+'-'+DD+'T'+HH1+':'+MM1+':00,'+YYYY+'-'+MM+'-'+DD+'T'+HH2+':'+MM2+':59'
                diaryString.write("\n"+diaryitem)
                print ("Diary "+diaryitem)
    return diaryString.getvalue()


# -------------------------------------------------------------------#


class Uto_Wetlabs(DataSource):
    description = "Utö Wetlabs"

    @staticmethod
    def detect(filename):
        f = open(filename, 'r')
        line1 = f.readline()
        line2 = f.readline()
        line3 = f.readline()
        f.close()
        sciencetimepattern = re.compile(
            r"\d\d\d\d-[01]\d-[0-3]\d [012]\d:[0-5]\d:[0-5]\d",
            re.IGNORECASE)
        if line1.startswith('"Ser#: ') and line2.startswith('"Period_start",') and sciencetimepattern.match(line3):
            return True
        return False

    @staticmethod
    def readFile(filename):
        result = TimeSeries()
        f = open(filename, 'r')
        line1 = f.readline()
        header = f.readline()
        f.close()

        result.filename = filename
        header = re.sub(r'[".]', '', header)                # Remove quotation marks
        header = re.sub('Period_start,', 'Time,', header)   # Rename time column
        header = re.sub(r'#', 'num_', header)               # Hash mark not allowed, replace with "_num"
        header = re.sub(r'_([a-zA-Z/]+)(,|$)', '[\\1]\\2', header) # "_unit" to "[unit]"
        result.header = header
        result.parse_header()
        result.read_data(filename, missing_values=-999.0000, skip_header=2)

        print ("Type of time column: {}".format(type(result.get_time_col())))
        result.time = result.get_time_col().astype('O')
        result.add_qc_cols()
        return result

    @staticmethod
    def readDefaultDiary():
        return Uto_Diary()


# -------------------------------------------------------------------#



class Serenade(DataSource):
    description = "Silja Serenade pCO2"

    @staticmethod
    def detect(filename):
        f = open(filename, 'r')
        line1 = f.readline()
        line2 = f.readline()
        line3 = f.readline()
        line4 = f.readline()
        f.close()
        if (not line1.startswith('5 header lines')
                or not line2.startswith('xCO2 surface underway data')
                or not line3.startswith('Collected beginning on:')):
            return False
        return True

    @staticmethod
    def readFile(filename):
        result = TimeSeries()
        f = open(filename, 'r')
        line1 = f.readline()
        line2 = f.readline()
        line3 = f.readline()
        line4 = f.readline()
        header = f.readline()
        f.close()
        header = re.sub(',', '_', header)   # make sure no commas in column names
        header = re.sub('\t', ', ', header) # change tabs to commas
        header = re.sub('\(', '[', header) # ()s to []s for units
        header = re.sub('\)', ']', header) #

        result.header = header
        result.parse_header(datetime=[['time', 'U6'], ['date', 'U8']])
        print(result.dtypes)
        result.read_data(filename, missing_values='NaN',
            delimiter='\t', skip_header=5)

        # Form a list of unified ISO 8601 time strings
        date = [ D[:4]+"-"+D[4:6]+"-"+D[6:8] for D in result.data['Date']]
        time = [ T[:2]+":"+T[2:4]+":"+T[4:6] for T in result.data['Time']]
        npdatetimes = [np.datetime64(S[0]+'T'+S[1], 's') for S in zip(date, time)]

        # Properly remove both the Date and Time columns
        result.remove_col('Date')
        result.remove_col('Time')

        # Add our new standard time column with both date and time
        result.append_col(name='Time', dtype='datetime64[s]', data=npdatetimes,
            timecol=True)

        print ("Type of time column: {}".format(type(result.get_time_col())))
        result.time = result.get_time_col().astype('O')
        result.add_qc_cols()
        return result

dataSources = [ TestDataSource, Uto_MicroTSG_DS, Uto_Wetlabs, Serenade ]


# class DevNoptelLaser(DataSource):
#     type = 'Noptel laser'
#
#     def readFile(self, filename):
#         result = TimeSeries()
#
#
#         return result
