class Instrument(object):
    """Base class for different instruments.
    
    Attributes
    ----------
    description : str
        Short description text for the instrument
    
    """
    
    description = None
    
    def __init__(self):
        # BODC vocabularies
        self.parameter = None
        self.unit = None
        # Measurement interval & data averaging
        self.interval = None
        self.average = None
        # Possible calibration parameters
        self.offset = None
        self.scaling = None
        # Instrument ID
        self.id = None
    
    def __str__(self):
        return '{} {}'.format(type(self).description, self.id)

    def read_data(self):
        """Read and parse the instrument-specific data file.
        
        Parameters
        ----------
        fname : str
            File path for the data file
        
        Returns
        -------
        ts : TimeSeries
            A TimeSeries object for the internal data handling
        
        """
        
        pass

    def check_data(self):
        """Do the automatic data quality checks and assign quality flags."""
        
        pass

    