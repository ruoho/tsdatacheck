"""Module for helper classes, functions and constants.

"""

import os
import sys

from configparser import ConfigParser
from datetime import datetime

ROOT_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')

def read_ini(fpath):
    """Read the configuration file and return the option values as a
    ConfigParser section.
    
    """
    
    parser = ConfigParser(allow_no_value = True)
    parser.optionxform = str
    parser.read(fpath)
    return parser

